use std::io::{Read, Write};
use std::net::{SocketAddr, TcpStream};
use std::str;

fn main() {
    let port = 3000;
    let addr = SocketAddr::from(([127, 0, 0, 1], port));
    let mut stream = TcpStream::connect(&addr).expect("Failed to connect to address");
    stream.write("Hello World".as_bytes()).unwrap();
    let mut buffer = [0; 11];
    stream.read(&mut buffer).unwrap();

    println!(
        "Got response from server {:?}",
        str::from_utf8(&buffer).unwrap()
    )
}

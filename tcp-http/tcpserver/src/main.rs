use std::io::{Read, Write};
use std::net::{SocketAddr, TcpListener};

fn main() {
    let port = 3000;
    let addr = SocketAddr::from(([127, 0, 0, 1], port));
    let connection_listener = TcpListener::bind(&addr).expect("Failed to bind address");

    println!("🚀 Running on port {}", port);

    for stream in connection_listener.incoming() {
        let mut stream = stream.unwrap();
        println!("⚡️ Connection established");

        let mut buffer = [0; 1024];
        stream
            .read(&mut buffer)
            .expect("Failed to read from incoming stream");
        stream.write(&mut buffer).unwrap();
    }
}
